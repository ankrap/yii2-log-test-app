<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class TestController extends Controller
{
    
    public function actionTest()
    {
        echo 'TEST!';
    }

    public function actionInfo()
    {
        echo 'info send';
        Yii::info('info');
    }

    public function actionWarning()
    {
        echo 'warning send';
        Yii::warning('warning');
    }

    public function actionError()
    {
        echo 'error send';
        Yii::error('error');
    }
}
